package fr.isima.ppc;

import java.io.File;
import java.io.IOException;

public class App 
{
    public static void main( String[] args ) throws IOException
    {
        
        // System.out.println("Instance,Method,Result");
        // for (int i=1; i<11; i++){
        //     String fileName = "la" + String.format("%02d", i) + ".txt";
        //     File source = new File("/home/simon/Isima/zz3-constraint-programming/lab3-jobshop-sheduling/jobshop/instance/" + fileName);

        //     Jobshop jobshop = new Jobshop(source);
        //     System.out.print("la" + String.format("%02d", i));
        //     jobshop.solveIfThenElseModel();
        //     System.out.print("la" + String.format("%02d", i));
        //     jobshop.solveCumulativeModel();
        // }
        
        File source = new File("/home/simon/Isima/zz3-constraint-programming/lab3-jobshop-sheduling/jobshop/instance/la01.txt");
        Jobshop jobshop = new Jobshop(source);
        jobshop.solveIfThenElseModel();
        jobshop.solveCumulativeModel();
        

    }
}
