package fr.isima.ppc;


public class Job {

    public final int id;

    public Job(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }
}
