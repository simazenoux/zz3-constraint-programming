package fr.isima.ppc;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Task;


public class Jobshop {

    private ArrayList<Machine> machines;
    private ArrayList<Job> jobs;
    private ArrayList<Operation> operations;
    private int ub = 3000;
    private String duration = "60s";
    
    public Jobshop(File source) throws IOException{
        StreamTokenizer streamTokenizer = new StreamTokenizer(new FileReader(source));

        // Creating the job objects
        streamTokenizer.nextToken();
        final int JOB_COUNT = (int) streamTokenizer.nval;
        this.jobs = new ArrayList<>();
        for (int i=0; i<JOB_COUNT; i++){
            this.jobs.add(new Job(i));
        }

        // Creating the machine objects
        streamTokenizer.nextToken();
        final int MACHINE_COUNT = (int) streamTokenizer.nval;
        this.machines = new ArrayList<>();
        for (int i=0; i<MACHINE_COUNT; i++){
            this.machines.add(new Machine(i));
        }


        // Creating the operation objects
        this.operations = new ArrayList<>();
        for (int i=0; i<JOB_COUNT; i++){
            Job job = this.jobs.get(i);
            
            for (int j=0; j<MACHINE_COUNT; j++){
                streamTokenizer.nextToken();
                Machine machine = this.machines.get((int) streamTokenizer.nval);
       
                streamTokenizer.nextToken();
                int duration = (int) streamTokenizer.nval;
                Operation operation = new Operation(machine, job, duration);

                operations.add(operation);
            }
        }
    }

    @Override 
    public String toString(){
        return jobs.toString();
    }



    public Model solveCumulativeModel(){

        // Define the model
        Model model = new Model("Job Shop Cumulative");

        Map<Job, ArrayList<Task>> jobToTasks = new HashMap<>();
        for (Job job: jobs){
            jobToTasks.put(job, new ArrayList<Task>());
        }

        Map<Machine, Set<Task>> machineToTasks = new HashMap<>();
        for (Machine machine: machines){
            machineToTasks.put(machine, new HashSet<Task>());
        }

        for (Operation operation: operations) {
            Task task = model.taskVar(model.intVar(operation.toString(), 0, ub), operation.getDuration());
            jobToTasks.get(operation.getJob()).add(task);
            machineToTasks.get(operation.getMachine()).add(task);
        }

        // Precedence constraint
        for (Job job : jobs) {
            for (int i = 0; i < (jobToTasks.get(job).size() - 1); i++) {
                Task currentTask = jobToTasks.get(job).get(i);
                Task nextTask = jobToTasks.get(job).get(i+1);
                model.arithm(currentTask.getEnd(), "<=", nextTask.getStart()).post();
            }
        }


        // Disjunctive arcs constraint
        for (Machine machine: machines){
            Task[] tasksPerformedByTheMachine = new Task[machineToTasks.get(machine).size()];
            int i=0;
            for (Task task : machineToTasks.get(machine)){
                tasksPerformedByTheMachine[i] = task;
                i+=1;
            }
            model.cumulative(tasksPerformedByTheMachine, model.intVarArray(machineToTasks.get(machine).size(), 1, 1), model.intVar(1)).post();
        }
        
        // Define objective
        IntVar makespan = model.intVar("makespan", 0, ub);
        IntVar [] endFinalTasks = new IntVar[jobs.size()];
        int i = 0;
        for (Job job: jobs){
            Task lastTask = jobToTasks.get(job).get(jobToTasks.get(job).size()-1);
            endFinalTasks[i] = lastTask.getEnd();
            i++;
        }
        model.max(makespan, endFinalTasks).post();;


        // Solve the problem

        Solver solver = model.getSolver();
        solver.limitTime(duration);
        solver.showShortStatistics();
        Solution solution = solver.findOptimalSolution(makespan, Model.MINIMIZE);
        System.out.println(",Cumulative," + solution.getIntVal(makespan));
        return model;
    }

    public Model solveIfThenElseModel(){

        // Define the model
        Model model = new Model("Job Shop IfThenElse");

        Map<Job, ArrayList<Task>> jobToTasks = new HashMap<>();
        for (Job job: jobs){
            jobToTasks.put(job, new ArrayList<Task>());
        }

        Map<Machine, Set<Task>> machineToTasks = new HashMap<>();
        for (Machine machine: machines){
            machineToTasks.put(machine, new HashSet<Task>());
        }

        for (Operation operation: operations) {
            Task task = model.taskVar(model.intVar(operation.toString(), 0, ub), operation.getDuration());
            jobToTasks.get(operation.getJob()).add(task);
            machineToTasks.get(operation.getMachine()).add(task);
        }

        // Precedence constraint
        for (Job job : jobs) {
            for (int i = 0; i < (jobToTasks.get(job).size() - 1); i++) {
                Task currentTask = jobToTasks.get(job).get(i);
                Task nextTask = jobToTasks.get(job).get(i+1);
                model.arithm(currentTask.getEnd(), "<=", nextTask.getStart()).post();
            }
        }


        // Disjunctive arcs constraint
        for (Machine machine: machines){
            for (Task task1: machineToTasks.get(machine)){
                for (Task task2: machineToTasks.get(machine)){
                    if (task1 != task2){
                        // b[machine.getId()][position2] = model.boolVar("b_" + i + "_" + j + "_" + u + "_" + v);
                        model.ifThenElse(
                            model.boolVar(),
                            model.arithm(task1.getEnd(), "<=", task2.getStart()), 
                            model.arithm(task2.getEnd(), "<=", task1.getStart())
                        );
                    }
                } 
            }
        }
        
        // Define objective
        IntVar makespan = model.intVar("makespan", 0, ub);
        IntVar [] ends = new IntVar[jobs.size()];
        int i = 0;
        for (Job job: jobs){
            Task lastTask = jobToTasks.get(job).get(jobToTasks.get(job).size()-1);
            ends[i] = lastTask.getEnd();
            i++;
        }
        model.max(makespan, ends).post();;

        
        Solver solver = model.getSolver();
        solver.limitTime(duration);
        solver.showShortStatistics();
        Solution solution = solver.findOptimalSolution(makespan, Model.MINIMIZE);
        System.out.println(",IfThenElse," +solution.getIntVal(makespan));

        // Print the solution
        // System.out.println("Solution: ");
        // for (int i = 0; i < operations.size(); i++) {
        //     System.out.print("Operation " + i + ": start=" + starts[i].getValue() + ", end=" + ends[i].getValue());
        // }

        // System.out.println("Result per job :");
        // for (Job job: jobs){
        //     for (Task task: jobToTasks.get(job)){
        //         System.out.print("("+task.getStart().getValue() + "-" + task.getEnd().getValue()+")");
        //     }
        //     System.out.println("");
        // }

        
        return model;
    }
}
