package fr.isima.ppc;

public class Machine {
    private int id;


    public Machine(int id){
        this.id = id;
    }


    public int getId(){
        return this.id;
    }

    @Override
    public String toString() {
        return String.valueOf(this.id);
    }
}
