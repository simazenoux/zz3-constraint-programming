package fr.isima.ppc;

public class Operation {
    
    private Machine machine;
    private Job job;
    private int duration;

    public Operation(Machine machine, Job job, int duration){
        this.machine = machine;
        this.job = job;
        this.duration = duration;
    }

    public Machine getMachine(){
        return this.machine;
    }

    public Job getJob(){
        return this.job;
    }

    public int getDuration(){
        return this.duration;
    }

    @Override
    public String toString() {
        return "(Machine=" + machine.getId() + ", Job" + job.getId() + ", Duration=" + duration + ")";
    }
}
