package fr.isima.ppc;

import java.util.HashMap;
import java.util.Map;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.*;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;

public class App 
{
    public static void main( String[] args )
    {
        // Creating countries
        final Country PORTUGAL = new Country("Portugal");
        final Country SPAIN = new Country("Spain");
        final Country FRANCE = new Country("France");
        final Country GERMANY = new Country("Germany");
        final Country BELGIUM = new Country("Belgium");
        final Country ICELAND = new Country("Iceland");


        // Creating the country adjacency graph
        ImmutableGraph<Country> countryAdjacencyGraph =
            GraphBuilder.undirected()
            .<Country>immutable()
            .putEdge(PORTUGAL, SPAIN)
            .putEdge(SPAIN, FRANCE)
            .putEdge(FRANCE, GERMANY)
            .putEdge(FRANCE, BELGIUM)
            .putEdge(GERMANY, BELGIUM)
            .addNode(ICELAND)
            .build();


        // Creating the model
        Model model = new Model("Graph coloring problem");
        

        int number_of_colors = 3;

        // Creating variables
        Map<Country, IntVar> map = new HashMap<>();
        for (Country country: countryAdjacencyGraph.nodes()){
            map.put(country, model.intVar(country.toString(), 1, number_of_colors));
        }


        // Creating constraints
        for (Country country: countryAdjacencyGraph.nodes()){
            for(Country adjacentCountry: countryAdjacencyGraph.adjacentNodes(country)){
                model.arithm(map.get(country), "!=", map.get(adjacentCountry)).post();;
            }
        }

        // Solving the problem
        Solution solution = model.getSolver().findSolution();
        if (solution != null){
            System.out.println(solution.toString());
        }
    }
}
