package fr.isima;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.variables.*;

public class App 
{
    public static void main(String[] args) {
        int n=8;
        
        // Model
        Model model = new Model("N-queens problem");
        
        // Variables
        IntVar[] vars = new IntVar[n];
        for (int i=0; i<n; i++)
        {
            vars[i] = model.intVar("Q_"+i, 1, n);
        }
 
        // Constraints
        for (int i=0; i<n-1; i++){
            for (int j=i+1; j<n; j++){
                model.arithm(vars[i], "!=", vars[j]).post();
                model.arithm(vars[i], "!=", vars[j], "-", j-i).post();
                model.arithm(vars[i], "!=", vars[j], "+", i+j).post();

            }
        }
        
        System.out.println(model.toString());
        
        Solution solution = model.getSolver().findSolution();
        if (solution != null){
            System.out.println(solution.toString());
        }
    }
}
